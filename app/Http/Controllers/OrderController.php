<?php

namespace App\Http\Controllers;

use App\Order;
use App\Orderdetail;
use App\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
//use Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        $items = Item::all();
        
        return view('pages.order')->withOrders($orders)->withItems($items);

        $name = customer::get()->pluck('name', 'id');
        $item_id = item::get()->pluck('itemname', 'id');
        return view('pages.order')->withName($name)->withItemId($item_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order;
        $order->orderdate = Carbon::now();
        $order->customer_id = Auth::user()->id;
        $order->save();

        $itemname = $request->input('item_id');
        $quantities = $request->input('quantity');

        $od = new Orderdetail;

        $od->order_id = $order->id;
        // dd($request->item_id);
        $od->item_id = $request->item_id;
        $od->quantity=1;

        $od->save();

        return redirect() -> back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
