<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $fillable = [
        'itemname',
        'platform',
        'genre',
        'summary',
        'cover',
        'quantity',
        'price',
	];

    public function orders(){
        return $this->belongsToMany('App\Orderdetail');
    }

}
