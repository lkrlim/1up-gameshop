<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
    	'customer_id',
		'deliverydate',
	];

    public function customers(){
        return $this->belongsTo('App\Customer');
    }

    public function orderdetails(){
        return $this->hasMany('App\Orderdetail');
    }
}
