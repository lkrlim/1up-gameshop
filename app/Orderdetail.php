<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderdetail extends Model
{
    protected $table = 'order_details';
    protected $fillable = [
    	'order_id',
		'item_id',
		'quantity',
	]; 

    public function orders()
    {
        return $this->belongsTo('App\Order');
    }

    public function items()
    {
        return $this->hasMany('App\Item');
    }
}
