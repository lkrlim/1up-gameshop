@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                    <hr>
                    <button type="submit" class="btn btn-primary" onclick="location.href = '/';">
                            Go back to Home page
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection