@extends('pages.main')

@section('stylesheet')
	{!! Html::style('css/index.css') !!}
	{!! Html::style('css/fonts.css') !!}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@stop

@section('body')
	<div class="container hero">
		<div class="title">GAME NOT OVER</div>
		<i id="subtitle">buy games from our store now</i>
		<div class="buttons">
			{{-- <button class="btns" onclick="location.href = '/login';">login</button>
			<button class="btns" onclick="location.href = '/register';">signup</button> --}}
			@if (Auth::guest())
				<button class="btns" onclick="location.href = '/login';">login</button>
				<button class="btns" onclick="location.href = '/register';">signup</button>
			@else
				<button class="btns" onclick="location.href = '/orders';">shop now</button>
				{{-- {!! Form::open(['route' => ['users.show', Auth::user()->id], 'method' => 'GET']) !!}
		            {!! Form::button('my orders', ['type' => 'submit' ,'class' => 'btns']) !!}
		        {!! Form::close() !!} --}}
			@endif
		</div>


	</div>
	<div class="container products" id="products">
		<div class="row col-md-" id="header"> <center>We Sell and Deliver games on these platforms</center> </div>
		<div class="row col-md-12 plats" >
			<center>
				<div class="plat-container">
					<img src="{{ URL::asset('img/pc.png')}}" class="md-4 platform">
					<h4 id="subheader">PC</h4>
				</div>
				<div class="plat-container">
					<img src="{{ URL::asset('img/handheld.png')}}" class="md-4 platform">
					<h4 id="subheader">Handheld</h4>
				</div>
				<div class="plat-container">
					<img src="{{ URL::asset('img/console.png')}}" class="md-4 platform">
					<h4 id="subheader">Console</h4>
				</div>
			</center>
		</div>
	</div>
	<div class="smallcontainer" id="about">
		<div class="separator"></div>
		<div class="darkbox">
			<p>1Up Gameshop delivers games anywhere in Davao City within 7 days.
			The store aims to let all gamers in the city experience games at a lower price
			than most stores in the city. </p>
			<p>solely developed by Lianne Lim</p>
			<p>Admin account: admin@admin.com, pw: adminadmin</p>
		</div>
	</div>
	<div class="smallcontainer" id="contact">\
		<div class="separator"></div>
		<div class="darkbox pull-right" style="margin-right: 100px">
			<table>
			<td>
				<tr><h2><i class="fa fa-phone" aria-hidden="true"></i> 0912 345 6789</h2></tr>
				<tr><h2><i class="fa fa-map-marker" aria-hidden="true"></i> Jacinto st. Davao City</h2></tr>
				<tr><h2><i class="fa fa-envelope" aria-hidden="true"></i> 1up-games@1up.com</h2></tr>

			</td>
		</table>
		</div>
	</div>
	<footer style="color: #aaa">
		<center><p>incomplete due to lacking time and load of schoolwork :(</p></center>
	</footer>
@stop
