@extends('pages.main')

@section('stylesheet')
  {!! Html::style('css/styles.css') !!}
  {!! Html::style('css/styles.css') !!}
@stop

@section('body')
  <div class="container itempage">
    <div class="separator"></div>
    <div class="separator">
        <button class="btns pull-right" onclick="location.href = '/newitem';">Add new game</button>
    </div>
    <table class="table itemtable">
        <thead>
            <tr class="filters">
                {{-- <th><input type="text" class="form-control" placeholder="Last Name" disabled></th>
                <th><input type="text" class="form-control" placeholder="First Name" disabled></th>
                <th><input type="text" class="form-control" placeholder="Address" disabled></th> --}}
                <th>Cover</th>
                <th>Title</th>
                <th>Description</th>
                <th>Platform</th>
                <th>Genre</th>
                <th>Quantity</th>
                <th>delete?</th>
                <th>edit?</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($items as $item)
            <tr>
            	{{-- <td class="coverimg">{{ $item->cover }}</td>  --}}
                <td><img src="data:image/jpeg;base64,{{ base64_decode($item->cover)}}" /></td>
                <td>{{ $item->itemname }}</td> 
                <td style="max-width: 200px">{{ $item->summary }}</td> 
                <td>{{ $item->platform }}</td> 
                <td>{{ $item->genre }}</td> 
                <td>{{ $item->quantity }}</td> 
                {!! Form::open(['route' => ['items.destroy', $item->id], 'method' => 'DELETE']) !!}
                    <td>{!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit' ,'class' => 'btn btn-danger']) !!}</td>
                {!! Form::close() !!}
                {{-- <td>{!! Form::button('<i class="glyphicon glyphicon-edit"></i>', ['type' => 'submit' ,'class' => 'btn btn-warning']) !!}</td> --}}
                <td><button class="btn btn-warning" type="submit"><i class="glyphicon glyphicon-edit"></i></button></td>


                {{-- <td class="text-right">
                {!! Form::open(['route' => ['customers.destroy', $customer->id], 'method' => 'DELETE']) !!}
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit' ,'class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
                </td> --}}
            </tr>

           
            @endforeach
        </tbody>
    </table>
  </div>
@stop
