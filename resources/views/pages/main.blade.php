<!DOCTYPE html>
<html lang="en">
<head>
	@include('partials._head')
</head>
	<body>
		@include('pages.navbar')

		@yield('body')

		@include('partials._scripts')
	</body>
</html>