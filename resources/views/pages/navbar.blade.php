<div class="nav">
	<a href="/"><img src="{{ URL::asset('img/1up-white.png')}}" class="logo"></a>
	<span class="menu" onclick="openNav()">&#9776;</span>
</div>

<div id="myNav" class="overlay">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="overlay-content">
  	<a href="/">Home</a>
    <a href="/#about">About</a>
    <a href="/#contact">Contact</a>
    @if (Auth::guest())
		<a href="/login">Login</a>
   		<a href="/register">Register</a>
	@else
		@if (Auth::user()->name == "Administrator" AND Auth::user()->email == "admin@admin.com")
	   		<a href="/items">Items</a>
	   	@endif
		<a href="/users/{{ Auth::user()->id }}"><i>{{{ isset(Auth::user()->name) ? Auth::user()->name : "" }}}</i></a>
		<a href="/logout"><i>Logout</i></a>
	@endif
  </div>
</div>

<script>
	function openNav() {
	    document.getElementById("myNav").style.height = "100%";
	}

	function closeNav() {
	    document.getElementById("myNav").style.height = "0%";
	}
</script>
