@extends('pages.main')

@section('stylesheet')
  {!! Html::style('css/styles.css') !!}
@stop

@section('body')
  <div class="container itempage">
    <div class="separator"></div>
     <div class="newitem">
     	{!! Form::open(['route' => 'items.store', 'class' => 'newitem']) !!}
	        {!! Form::label('itemname', 'Game Title:') !!}
	        {!! Form::text('itemname', null, array('class' => 'form-control in', 'required' => '')) !!}
	        {!! Form::label('platform', 'Platform') !!}
	        {!! Form::text('platform', null, array('class' => 'form-control in', 'required' => '')) !!}
	        {!! Form::label('genre', 'Genre') !!}
	        {!! Form::text('genre', null, array('class' => 'form-control in', 'required' => '')) !!}
	        {!! Form::label('summary', 'Summary') !!}
	        {!! Form::textarea('summary', null, array('class' => 'form-control in', 'required' => '', 'rows' => '3', 'maxlength' => '190', 'resize' => 'none')) !!}
	        {!! Form::label('quantity', 'Quantity') !!}
	        {!! Form::number ('quantity', null, array('class' => 'form-control in', 'required' => '', 'min' => '0')) !!}
	        {!! Form::label('price', 'Price (PHP)') !!}
	        {!! Form::number('price', null, array('class' => 'form-control in', 'required' => '', 'min' => '0')) !!}
	        {!! Form::label('cover', 'Cover') !!}
	        {!! Form::file ('cover', null, array('class' => 'form-control btns')) !!}
	        {!! Form::submit('Save to inventory', array('class' => 'btn btn-success pull-right', 'style' => 'margin-top: 20px')) !!}
	      {!! Form::close() !!}
     </div>
@stop


{{-- itemname
platform
genre
summary
cover
quantity
price
 --}}