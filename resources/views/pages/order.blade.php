@extends('pages.main')

@section('stylesheet')
     {!! Html::style('css/styles.css') !!}
@stop

@section('body')
    <div class="container orderpage">
        <div class="separator"></div>
        <div class="content">
            {!! Form::open(['method'=>'GET','url'=>'orders', 'class'=>'navbar-form navbar-left','role'=>'search'])  !!}
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control searchbar" style="width: 80vw;" name="search" placeholder="Type a game title...">
                    <span class="input-group-btn">
                        <button class="btn btn-default-sm" type="submit">
                            <i class="fa fa-search"><!--<span class="hiddenGrammarError" pre="" data-mce-bogus="1"--> Search</i>
                        </button>
                    </span>
                </div>
            {!! Form::close() !!}
        </div>

        <div class="content">
            <table class="table itemtable">
                <thead>
                    <tr class="filters">
                        {{-- <th><input type="text" class="form-control" placeholder="Last Name" disabled></th>
                        <th><input type="text" class="form-control" placeholder="First Name" disabled></th>
                        <th><input type="text" class="form-control" placeholder="Address" disabled></th> --}}
                        <th style="width: 150px">Cover</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Platform</th>
                        <th>Genre</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Buy now!</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($items as $item)
                    <tr>
                            <td><img src="data:image/jpg;base64,{{ base64_decode($item->cover)}}" /></td>
                            <td>{{ $item->itemname }}</td> 
                            <td style="max-width: 200px">{{ $item->summary }}</td> 
                            <td>{{ $item->platform }}</td> 
                            <td>{{ $item->genre }}</td> 
                            <td>{{ $item->quantity }}</td> 
                            <td>{{ $item->price }} PHP</td> 
                            {{--     --}}
                            <td>{!! Form::open(['route' => 'orders.store', 'method' => 'POST']) !!}
                                    {!!Form::hidden('item_id',$item->id)!!}
                                    {!! Form::button('<i class="glyphicon glyphicon-chevron-right"></i>', ['type' => 'submit' ,'class' => 'btn btn-warning']) !!}
                                {!! Form::close() !!}</td>
                    </tr>                       
                    @endforeach
                </tbody>
            </table>
        </div>



        {{-- <div class="row">
            <div class="col-lg-12">
              <div class="col-md-12 form-group">
                {!! Form::open(['route' => 'orders.store']) !!}
                <h3>Order Details</h3>
                <table class="table table-bordered table-hover" id="dataTable">
                  <thead>
                      <tr>
                          <th>Item Name</th>
                          <th>Quantity</th>
                      </tr>
                  </thead>
                  <tbody>
                        {{ Form::label('order_date', 'Order Date:') }}
                        {{ Form::date('order_date', \Carbon\Carbon::now(), array('class' => 'form-control', 'required' => '','readonly' => 'readonly' )) }}
                        
                      <tr>
                        <td class="col-md-10">
                             replace ['value1', 'value2'] with your array variable from your controller
                            {!! Form::select('item_id[]', $item_id, null, ['class' => 'form-control', 'required' => '', 'placeholder' => 'Select One']) !!}
                        </td>
                        <td>
                            {!! Form::number('quantity[]', null, ['class' => 'form-control', 'required' => '']) !!}
                        </td> 

                      </tr>
                  </tbody>
                </table>
                <input type="button" value="Add Item" class="btn btn-primary" onClick="addRow('dataTable')" /> 
                <input type="button" value="Remove Item" class="btn btn-danger pull-right" onClick="deleteRow('dataTable')" /> 
                <br>
                {!! Form::submit('Save Order', ['class' => 'btn btn-success form-control', 'required' => '', 'style' => 'margin-top:20px; ']) !!}
            </div>
            {!! Form::close() !!}
        </div> --}}
    </div>

    {!! Html::script('js/dynamic_table.js') !!}
@stop