@extends('pages.main')

@section('stylesheet')
	{!! Html::style('css/nav.css') !!}
	{!! Html::style('css/fonts.css') !!}
	{!! Html::style('css/user.css') !!}
@stop

@section('body')
	<div class="container">
		<div class="separator"></div>
		<div class="content">
			<h2 id="name"> {{ $users->name }}</h2>
			{!! Form::open(['route' => ['users.show', $users->id], 'method' => 'GET']) !!}
			<div class="row">
				<table class="table table-striped table-condensed userinv">
					<thead>
						<tr>
							<th style="width: 150px">Cover</th>
	                        <th>Title</th>
	                        <th>Description</th>
	                        <th>Platform</th>
	                        <th>Genre</th>
	                        <th>Delivery date</th>
							{{-- <th>{{ $orderdetails }}</th> --}}
						</tr>
					</thead>   
					<tbody>
					@foreach($orders as $order)
	    				@if($users->id == $order->customer_id)
							@foreach($orderdetails as $orderdetail)
								@if($orderdetail->order_id !=null && $order->id == $orderdetail->order_id)
									@foreach($items as $item)
										<tr>
										@if($item->id == $orderdetail->item_id)
											{{-- <td><img src="data:image/jpg;base64,{{ base64_decode($item->cover)}}" style="max-width: 0px"/></td> --}}
											<td>{{ $item->cover }}</td>
											<td>{{ $item->itemname }}</td> 
					                        <td style="max-width: 250px">{{ $item->summary }}</td> 
					                        <td>{{ $item->platform }}</td> 
					                        <td>{{ $item->genre }}</td> 
					                        {{-- <td>{{ $order->orderdate-> }}</td> --}}
					                        <td>{{ Carbon\Carbon::parse($order->orderdate)->addDays(7)-> format('M-d-Y')}}</td>
										@endif
										</tr>
									@endforeach
								@endif
							@endforeach
						@endif
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@stop