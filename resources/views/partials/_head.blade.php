<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>1Up Gameshop @yield('title')</title>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

{!! Html::style('css/styles.css') !!}
@yield('stylesheet')